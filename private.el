;; ;; selection
;; (set-face-attribute 'region nil :background "#99bdff" :foreground "#0f215e")
;; ;; vertical border
;; (set-face-attribute 'vertical-border nil :background "gray15" :foreground "gray15")
;; 
;; ;; (set-face-attribute 'whitespace-space nil :background nil)
;; ;;
;; ;; ;; fonts
;; (set-face-attribute 'font-lock-keyword-face nil :weight 'bold)
;; (set-face-attribute 'font-lock-constant-face nil :foreground "#fec66c" :weight 'normal)
;; (set-face-attribute 'font-lock-warning-face nil :underline '(:color "#885522" :style wave))
;; (set-face-attribute 'font-lock-string-face nil :foreground "#66915C")
;; ;;
;; ;; ;;org mode
;; (set-face-attribute 'org-block-begin-line nil :background  "#252525" :foreground "#444444")
;; (set-face-attribute 'org-block-end-line nil :background  "#252525" :foreground "#444444")
;; ;(set-face-attribute 'org-date nil :foreground "cyan4" :underline t)
;; ;(set-face-attribute 'org-done nil :foreground "#4EA55F" :weight 'bold)
;; ;(set-face-attribute 'org-todo nil :foreground "#AB5757" :weight 'bold)
;; ;;
;; ;; ;; style customizations for tommorow night theme
;; ;; ;;(set-face-attribute 'whitespace-space nil :foreground  :background nil)
;; (set-face-attribute 'org-block-begin-line nil :foreground "#414344" :background "#151719")
;; (set-face-attribute 'org-block-end-line nil :foreground "#414344" :background "#151719")
;; ;; ;; (set-face-attribute 'org-block nil :background nil)
;; ;; (set-face-attribute 'org-special-keyword nil :foreground "#55443C")
;; ;; (set-face-attribute 'font-lock-comment-face nil :background nil :foreground "gray40")
;; ;;(set-face-attribute 'org-meta-line nil :foreground "gray30")
;; ;;(set-face-attribute 'header-line nil :foreground "#1B2B28" :background "#93949F")
;; ;;(set-face-attribute 'helm-candidate-number nil :foreground "#472222")
;; (set-face-attribute 'mode-line nil :background "#93949F" :foreground "#16161B" :box nil) ;;'(:line-width 1 :color "#24272B")
;; (set-face-attribute 'mode-line-buffer-id nil :background "#313246" :foreground "#1B1C23")
;; (set-face-attribute 'mode-line-highlight nil :background "#8D93CE" :foreground "#252630")
;; (set-face-attribute 'mode-line-inactive nil :background "#595d60" :foreground "#191A20")
;; 
;; ;; (set-face-attribute 'company-tooltip nil :background "#888e93" :foreground "#2c2e38")
;; ;; (set-face-attribute 'company-tooltip-common nil :background "#5e2121" :foreground "#cc6666")
;; ;;
;; 
