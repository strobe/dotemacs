;;; -*- lexical-binding: t -*-

(defun tangle-init ()
  "If the current buffer is 'init.org' the code-blocks are
tangled, and the tangled file is compiled."
  (when (equal (buffer-file-name)
               (expand-file-name (concat user-emacs-directory "init.org")))
    ;; Avoid running hooks when tangling.
    (let ((prog-mode-hook nil))
      (org-babel-tangle)
      (byte-compile-file (concat user-emacs-directory "init.el")))))

(add-hook 'after-save-hook 'tangle-init)

(add-hook
 'after-init-hook
 (lambda ()
   (let ((private-file (concat user-emacs-directory "private.el")))
     (when (file-exists-p private-file)
       (load-file private-file)))))

(add-to-list 'load-path "/usr/local/Cellar/mu/1.2.0_1/share/emacs/site-lisp/mu/mu4e")

(setq byte-compile-warnings '(cl-functions))
(require 'cl)
(require 'package)

;; (if (version< emacs-version "27")
;; (message (package-initialize))
;;  nil))

;; https://github.com/kiwanami/emacs-epc/issues/35

;; bugfix
;; https://www.reddit.com/r/emacs/comments/cdei4p/failed_to_download_gnu_archive_bad_request/
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))

;;(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)

(let ((default-directory (concat user-emacs-directory "site-lisp/")))
  (when (file-exists-p default-directory)
    (setq load-path
          (append
           (let ((load-path (copy-sequence load-path)))
             (normal-top-level-add-subdirs-to-load-path)) load-path))))

(let ((default-directory (concat user-emacs-directory "elpa/")))
  (when (file-exists-p default-directory)
    (setq load-path
          (append
           (let ((load-path (copy-sequence load-path)))
             (normal-top-level-add-subdirs-to-load-path)) load-path))))

;; This is only needed once, near the top of the file
(eval-when-compile
  ;; Following line is not needed if use-package.el is in ~/.emacs.d
  ;(add-to-list 'load-path "/Users/evgeniy/.emacs.default/site-lisp/use-package")
  ;(add-to-list 'load-path "/Users/evgeniy/.emacs.default/elpa/all-the-icons-20200823.1605")
  (require 'use-package))


;;(eval-when-compile
   ;; Following line is not needed if use-package.el is in ~/.emacs.d
;;   (require 'use-package))

 (use-package smooth-scrolling :init (setq smooth-scroll-margin 5
                                           scroll-conservatively 101
                                           scroll-preserve-screen-position t
                                           auto-window-vscroll nil)
              :config (setq scroll-margin 5))

 ;;(use-package benchmark-init    ; Userful for emacs initialization speed debugging
 ;;  :config (add-hook 'after-init-hook 'benchmark-init/deactivate))

 (use-package yasnippet :config (yas-global-mode 1))  ; Snippets everyvery

 (use-package smartparens-config
   :ensure smartparens
   :config (progn (show-smartparens-global-mode t)))

 (add-hook 'prog-mode-hook 'turn-on-smartparens-mode)
 (add-hook 'markdown-mode-hook 'turn-on-smartparens-mode)

(when (memq window-system '(mac ns))
  (setq ns-pop-up-frames nil
        mac-option-modifier 'meta
        mac-command-modifier 'super
        x-select-enable-clipboard t)
  (exec-path-from-shell-initialize)
  (when (fboundp 'mac-auto-operator-composition-mode)
    (mac-auto-operator-composition-mode 1)))

(add-to-list 'default-frame-alist '(ns-appearance . dark))
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))

(setq ns-use-native-fullscreen nil)

(if (executable-find "trash")
    (defun system-move-file-to-trash (file)
      "Use `trash' to move FILE to the system trash.
      Can be installed with `brew install trash', or `brew install osxutils`''."
      (call-process (executable-find "trash") nil 0 nil file))
  ;; regular move to trash directory
  (setq trash-directory "~/.Trash/emacs"))

;;(autoload 'dash-at-point "dash-at-point"
;;  "Search the word at point with Dash." t nil)

(defun pbpaste ()
  (interactive)
  (shell-command-on-region
   (point)
   (if mark-active (mark) (point))
   "pbpaste" nil t))

(defun pbcopy ()
  (interactive)
  (call-process-region (point) (mark) "pbcopy")
  (setq deactivate-mark t))

(defun pbcut ()
  (interactive)
  (pbcopy)
  (delete-region (region-beginning) (region-end)))

(setq mac-pass-command-to-system t)

(require 'ox-latex)                 ; the latex-exporter (from org)
(require 'tramp)                    ; for projectile
(require 'smtpmail)
(require 'mu4e)
(require 'all-the-icons)
(require 'move-text)
(require 'posframe)
(require 'treemacs)
;;(require 'which-key-posframe)

(require 'auto-compile) ; idle-require
;;(require 'jedi)       ; auto-completion for python
(require 'ox-md)        ; Markdown exporter (from org)
(require 'tex-mode)     ; TeX, LaTeX, and SliTeX mode commands
(require 'recentf)      ; recently opened files
(require 'neotree)
(require 'epresent)
;;(require 'popwin)       ; window manager for Emacs which makes you free from the hell of annoying buffers such like *Help*
 ;;(require 'idle-require)             ; Need in order to use idle-require
 ;;(dolist (feature
 ;;         '(auto-compile             ; auto-compile .el files
 ;;           ;;jedi                     ; auto-completion for python
 ;;           ox-md                    ; Markdown exporter (from org)
 ;;           tex-mode                 ; TeX, LaTeX, and SliTeX mode commands
 ;;           recentf                  ; recently opened files
 ;;           neotree
 ;;           epresent
 ;;           ;;w3m
 ;;           ;;tabbar
 ;;           ;;helm-w3m
 ;;           ))
 ;;  (idle-require feature))
 ;;(setq idle-require-idle-delay 10)
 ;;(idle-require-mode 1)

(load "server")
(unless (server-running-p) (server-start))

;; (let ((user-home-dir "/Users/evgeniy")(user-emacsd-dir "/Users/evgeniy/Dropbox/dev/96_configs/new_emacs/.emacs.d/"))
;;   (progn
;;     ((setq )etq user-emacs-directory
;;      (expand-file-name user-emacsd-dir "Dropbox/dev/96_configs/new_emacs/.emacs.d/"))
;;     (setenv "HOME"
;;         (expand-file-name user-home-dir))
;;     (setq abbreviated-home-dir (concat
;;                                 "\\`"
;;                                 (getenv "HOME")
;;                                 "\\(/\\|\\'\\)"))
;;     (setq abbrev-file-name
;;           (locate-user-emacs-(format "message" format-args)ile "abbrev_defs"))
;;     (setq auto-save-list-file-prefix
;;           (locate-user-emacs-file "auto-save-list/.saves-"))
;;     (cd user-home-dir)))

(setq auto-revert-interval 15            ; Refresh buffers fast
      custom-file "~/.emacs.d/.emacs-custom.el"
      ;;custom-file (make-temp-file "")   ; Discard customization's
      default-input-method "TeX"        ; Use TeX when toggling input method
      echo-keystrokes 0.1               ; Show keystrokes asap
      inhibit-startup-message t         ; No Splash Screen Please
      initial-scratch-message nil       ; Clean Scratch Buffer
      recentf-max-saved-items 100       ; Show More Recent Files
      ring-bell-function 'ignore        ; Quiet
      cua-mode nil
                                        ;cua-selection-mode t              ; rectangle support, global mark mode, and other features
      sentence-end-double-space nil)    ; No double space
;; Some mac-bindings interfere with Emacs bindings.
(when (boundp 'mac-pass-command-to-system)
  (setq mac-pass-command-to-system nil))

(setq-default fill-column 79                    ; Maximum line width
              truncate-lines t                  ; Don't fold lines
              truncate-partial-width-windows nil
              indent-tabs-mode nil              ; Use spaces instead of tabs
              tab-width 4
              indent-line-function 'insert-tab  ; will insert spaces
              split-width-threshold 160         ; Split verticly by default
              split-height-threshold nil)       ; Split verticly by default
                                        ;auto-fill-function 'do-auto-fill ; Auto-fill-mode everywhere

;;  (let ((default-directory (concat user-emacs-directory "site-lisp/")))
;;    (when (file-exists-p default-directory)
;;      (setq load-path
;;            (append
;;             (let ((load-path (copy-sequence load-path)))
;;               (normal-top-level-add-subdirs-to-load-path)) load-path))))
;;
;;  (let ((default-directory (concat user-emacs-directory "elpa/")))
;;    (when (file-exists-p default-directory)
;;      (setq load-path
;;            (append
;;             (let ((load-path (copy-sequence load-path)))
;;               (normal-top-level-add-subdirs-to-load-path)) load-path))))

(fset 'yes-or-no-p 'y-or-n-p)

(defvar emacs-autosave-directory
  (concat user-emacs-directory "autosaves/")
  "This variable dictates where to put auto saves. It is set to a
  directory called autosaves located wherever your .emacs.d/ is
  located.")

;; Sets all files to be backed up and auto saved in a single directory.
(setq backup-directory-alist
      `((".*" . ,emacs-autosave-directory))
      auto-save-file-name-transforms
      `((".*" ,emacs-autosave-directory t)))

(set-language-environment "UTF-8")

(put 'narrow-to-region 'disabled nil)

;;(add-hook 'doc-view-mode-hook 'auto-revert-mode)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(defalias 'list-buffer 'ibuffer)
(defalias 'list-buffers 'ibuffer)
(defalias 'buffer-menu 'ibuffer)

(defvar-local is-new-file-buffer nil)

(defun save-new-file-before-kill ()
  (when (and (not (buffer-file-name))
             is-new-file-buffer
             (yes-or-no-p
              "New file has not been saved. Would you like to save before closing?"))
    (call-interactively 'save-buffer)))

(add-hook 'kill-buffer-hook 'save-new-file-before-kill)
(add-hook 'kill-emacs-hook 'save-new-file-before-kill)

(defun new-file (dir)
  ;;(interactive "DCreate New File In: ")
  (let ((buffer (generate-new-buffer "<Unsaved File>")))
    (switch-to-buffer buffer)
    (setq-local default-directory dir)
    (setq-local is-new-file-buffer t)))

(defun new-buffer ()
  (interactive)
  (new-file "/var/tmp"))

;;(define-key custom-bindings-map (kbd "s-N") 'new-buffer)

(dolist (mode
         '(tool-bar-mode                ; No toolbars, more room for text
           scroll-bar-mode              ; No scroll bars either
           blink-cursor-mode))          ; The blinking cursor gets old
  (funcall mode 0))

(dolist (mode
         '(abbrev-mode                  ; E.g. sopl -> System.out.println
           column-number-mode           ; Show column number in mode line
           delete-selection-mode        ; Replace selected text
           dirtrack-mode                ; directory tracking in *shell*
           ;;drag-stuff-global-mode       ; Drag stuff around
           global-company-mode          ; Auto-completion everywhere
           global-git-gutter-mode       ; Show changes latest commit
           global-prettify-symbols-mode ; Greek letters should look greek
           projectile-global-mode       ; Manage and navigate projects
           projectile-mode
           recentf-mode                 ; Recently opened files
           show-paren-mode              ; Highlight matching parentheses
           global-auto-revert-mode      ; reloading files if it changed on disk
           which-key-mode))             ; Available keybindings in popup
  (funcall mode 1))

(when (version< emacs-version "24.4")
  (eval-after-load 'auto-compile
    '((auto-compile-on-save-mode 1))))  ; compile .el files on save

;;(set-mouse-color "gray60")

;; [Only for the DOOM themes]
;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)
;; Enable custom neotree theme (all-the-icons must be installed!)
(doom-themes-neotree-config)
;; set theme color variant
(setq doom-gruvbox-dark-variant "hard")

;; load theme only at terminal
(defun is-in-terminal()
  (not (display-graphic-p)))

(if (is-in-terminal)
    ;; TERM theme
    (load-theme 'doom-monokai-pro t)
  ;; GUI theme
  (progn
    (load-theme 'doom-monokai-pro t)))

(defun cycle-themes ()
  "Returns a function that lets you cycle your themes."
  (lexical-let ((themes '#1=(doom-monokai-pro kaolin-valley-dark kaolin-dark doom-tomorrow-night kaolin-temple doom-spacegrey doom-oceanic-next doom-nord doom-molokai doom-gruvbox . #1#)))
    (lambda ()
      (interactive)
      ;; set theme color variant
      (setq doom-gruvbox-dark-variant "hard")
      ;; Rotates the thme cycle and changes the current theme.
      (load-theme (car (setq themes (cdr themes))) t))))

(cond
 ((member "Iosevka SS08" (font-family-list))
  (set-face-attribute 'default nil :font "Iosevka SS08-14"))
 ((member "Input" (font-family-list))
  (set-face-attribute 'default nil :font "Input-14"))
 ((member "Fira Code" (font-family-list))
  (set-face-attribute 'default nil :font "Fira Code-12"))
 ((member "Input" (font-family-list))
  (set-face-attribute 'default nil :font "InputMonoCompressed-12"))
 ((member "Hasklig" (font-family-list))
  (set-face-attribute 'default nil :font "Hasklig-12"))
 ((member "Inconsolata" (font-family-list))
  (set-face-attribute 'default nil :font "Inconsolata-12")))

(custom-set-faces
 '(default ((t (:height 140 :family "Iosevka SS08")))))

(set-face-attribute 'default nil :font "Iosevka SS08-14")
(set-frame-font "Iosevka SS08-14")

(defun cycle-fonts ()

  "Returns a function that lets you cycle your fonts."
  (lexical-let ((fonts '#1=("Iosevka SS08-18" "InputMonoCompressed-16" "Fira Code-16"
                            "Input-16" "Inconsolata-16" "Monoid-16"
                            "Hasklig-16"  . #1#)))
    (lambda ()
      (interactive)
      ;; Rotates the thme font and changes the current font.
      (let ((font (car (setq fonts (cdr fonts)))))
        (set-face-attribute 'default nil :font font)
        (message font)))))

(require 'whitespace)
(global-whitespace-mode t)
                                        ;(setq whitespace-style '(face tabs tab-mark spaces trailing))
(setq whitespace-style '(tabs tab-mark spaces trailing))

(set-face-underline  'whitespace-trailing t)
(set-face-background 'whitespace-trailing 'nil)

;; tabs
;;(custom-set-faces
;; '(whitespace-tab ((t (:foreground "#353535" :background nil)))))

;;(custom-set-faces
;; '(whitespace-tab
;;   ((((class color) (background dark)) (:background nil :foreground "#353535"))
;;    (((class color) (background light)) (:background nil :foreground "gray90"))
;;    (t (:inverse-video t)))))

(setq whitespace-display-mappings '(
                                    (tab-mark ?\t [?\u00BB ?\t] [?\\ ?\t])))       ; tab - left quote mark
;;(tab-mark 9 [124 9] [92 9]))               ; vertical line

(defmacro safe-diminish (file mode &optional new-name)
  `(with-eval-after-load ,file
     (diminish ,mode ,new-name)))

(diminish 'auto-fill-function)
(safe-diminish "eldoc" 'eldoc-mode)
(safe-diminish "flyspell" 'flyspell-mode)
(safe-diminish "helm-mode" 'helm-mode)
(safe-diminish "projectile" 'projectile-mode)
(safe-diminish "paredit" 'paredit-mode "()")

(require 'git-gutter-fringe)

(dolist (p '((git-gutter:added    . "#1a3a2a")
             (git-gutter:deleted  . "#3a1a1a")
             (git-gutter:modified . "#2a1122")))
  (set-face-foreground (car p) (cdr p))
  (set-face-background (car p) (cdr p)))

;;(set-face-attribute 'vertical-border nil :background "gray15" :foreground "gray15")

(setq-default prettify-symbols-alist '(("lambda" . ?λ)
                                       ("delta" . ?Δ)
                                       ("gamma" . ?Γ)
                                       ("phi" . ?φ)
                                       ("psi" . ?ψ)))

;;(set-face-attribute 'region nil :background "#99bdff" :foreground "#0f215e")

(add-hook 'pdf-tools-enabled-hook 'auto-revert-mode)
(add-to-list 'auto-mode-alist '("\\.pdf\\'" . pdf-tools-install))

(setq company-idle-delay 0
      company-echo-delay 0
      company-dabbrev-downcase nil
      company-minimum-prefix-length 2
      company-selection-wrap-around t
      company-transformers '(company-sort-by-occurrence
                             company-sort-by-backend-importance))

(setq company-tooltip-minimum-width 30)

(require 'helm)
   (require 'helm-config)

   (setq helm-split-window-inside-p t
         helm-M-x-fuzzy-match t
         helm-buffers-fuzzy-matching t
         helm-recentf-fuzzy-match t
         helm-semantic-fuzzy-match t
         helm-imenu-fuzzy-match    t
         helm-move-to-line-cycle-in-source t
         helm-multi-swoop-edit-save t
         projectile-completion-system 'helm)


   (setq helm-rg-default-directory 'git-root)

   (when (executable-find "rg")
        ;; For helm to recognize correctly the matches we need to enable
        ;; line numbers and columns in its output, something the
        ;; --vimgrep option does.
        (setq helm-grep-default-command         "rg --vimgrep --no-heading  %p %f"
              helm-grep-default-recurse-command "rg --vimgrep --no-heading %p %f"))

  (when (executable-find "ag")
     ;; For helm to recognize correctly the matches we need to enable
     ;; line numbers and columns in its output, something the
     ;; --vimgrep option does.
     (setq helm-grep-default-command         "ag --vimgrep --nogroup --nocolor -z %p %f"
           helm-grep-default-recurse-command "ag --vimgrep --nogroup --nocolor -z %p %f"))

   (set-face-attribute 'helm-selection nil :background "dark slate blue" :foreground "gray90")


   (helm-mode 1)
   (helm-projectile-on)
   (helm-adaptive-mode 1)

;;; /// tmp /// ;;;
  ;;(when (executable-find "ack")
   ;;  (setq helm-grep-default-command
   ;;        "ack -Hn --no-group --no-color %e %p %f"
   ;;        helm-grep-default-recurse-command
   ;;        "ack -H --no-group --no-color %e %p %f"))



;;  (when (executable-find "pt")
;;     ;; For helm to recognize correctly the matches we need to enable
;;     ;; line numbers and columns in its output, something the
;;     ;; --vimgrep option does.
;;     (setq helm-grep-default-command         "pt --nogroup --nocolor  %p %f"
;;           helm-grep-default-recurse-command "pt --nogroup --nocolor  %p %f"))
;;

(add-to-list 'display-buffer-alist
             `(,(rx bos "*helm" (* not-newline) "*" eos)
               (display-buffer-in-side-window)
               (inhibit-same-window . t)
               (window-height . 0.4)))

;; (setq helm-dash-browser-func 'eww)
;; (add-hook 'emacs-lisp-mode-hook
;;           (lambda () (setq-local helm-dash-docsets '("Emacs Lisp"))))
;; (add-hook 'erlang-mode-hook
;;           (lambda () (setq-local helm-dash-docsets '("Erlang"))))
;; (add-hook 'java-mode-hook
;;           (lambda () (setq-local helm-dash-docsets '("Java"))))
;; (add-hook 'haskell-mode-hook
;;           (lambda () (setq-local helm-dash-docsets '("Haskell"))))
;; (add-hook 'clojure-mode-hook
;;           (lambda () (setq-local helm-dash-docsets '("Clojure"))))

;(setq projectile-indexing-method 'alien)
                                        ;(setq projectile-indexing-method 'native)
(setq projectile-indexing-method 'hybrid)

(defun calendar-show-week (arg)
  "Displaying week number in calendar-mode."
  (interactive "P")
  (copy-face font-lock-constant-face 'calendar-iso-week-face)
  (set-face-attribute
   'calendar-iso-week-face nil :height 0.7)
  (setq calendar-intermonth-text
        (and arg
             '(propertize
               (format
                "%2d"
                (car (calendar-iso-from-absolute
                      (calendar-absolute-from-gregorian
                       (list month day year)))))
               'font-lock-face 'calendar-iso-week-face))))

(calendar-show-week t)

(setq calendar-week-start-day 1
      ;; calendar-latitude 60.0
      ;; calendar-longitude 10.7
      calendar-location-name "Yekaterinburg, Russia")

;; (setq mu4e-mu-binary "/usr/local/bin/mu")
(setq mu4e-maildir "~/.Mail")
(setq mu4e-drafts-folder "/[Google Mail]/Drafts")
;;(setq mu4e-sent-folder   "/[Google Mail]/Sent Mail")
;; don't save message to Sent Messages, Gmail/IMAP takes care of this
(setq mu4e-sent-messages-behavior 'delete)
;; allow for updating mail using 'U' in the main view:
(setq mu4e-get-mail-command "offlineimap")

;; something about ourselves
(setq
 user-mail-address "archjohn@gmail.com"
 user-full-name  "Evgeniy T"
 mu4e-compose-signature
 (concat
  "Cheers,\n"
  "Best Regards\n"))

;; ;; show images
(setq mu4e-view-show-images t)

;; use imagemagick, if available
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; attachment folder
(setq mu4e-attachment-dir  "~/Downloads")

;; view configuration
(setq mu4e-split-view 'vertical) ;; horizontal, vertical, single-window

;; convert html emails properly
;; Possible options:
;;   - html2text -utf8 -width 72
;;   - textutil -stdin -format html -convert txt -stdout
;;   - html2markdown | grep -v '&nbsp_place_holder;' (Requires html2text pypi)
;;   - w3m -dump -cols 80 -T text/html
;;   - view in browser (provided below)
;;(setq mu4e-html2text-command "w3m -dump -cols 80 -T text/html")
;; (setq mu4e-html2text-command "html2text -utf8 -width 80")
(setq mu4e-html2text-command "w3m -T text/html")
;; (setq mu4e-html2text-command "textutil -stdin -format html -convert txt -stdout")

;; nice view mode text formating
(add-hook 'mu4e-view-mode-hook
          (defun enable-olivetti () (olivetti-mode)))
;; spell check
(add-hook 'mu4e-compose-mode-hook
          (defun my-do-compose-stuff ()
            "My settings for message composition."
            (set-fill-column 72)
            (flyspell-mode)))

;; fetch mail every 5 mins
(setq mu4e-update-interval 900)

;; html view by default
(setq mu4e-view-prefer-html t)

;; configuration for sending mail
(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-stream-type 'starttls
      ;;smtpmail-stream-type 'ssl
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587)

;; configure open email in browser
(defun mu4e-action-view-in-w3m (msg)
  "View the body of the message in emacs w3m."
  (interactive)
  (w3m-browse-url (concat "file://"
                          (mu4e~write-body-to-html msg))))

(defun mu4e-action-view-in-eww (msg)
  "View the body of the message in emacs eww."
  (interactive)
  (eww-browse-url (concat "file://"
                          (mu4e~write-body-to-html msg))))

(add-to-list 'mu4e-view-actions '("browser" . mu4e-action-view-in-browser) t)
(add-to-list 'mu4e-view-actions '("w3m view" . mu4e-action-view-in-w3m) t)
(add-to-list 'mu4e-view-actions '("eww view" . mu4e-action-view-in-eww) t)


;; shortcuts
(setq mu4e-maildir-shortcuts
      '( ("/INBOX"               . ?i)
         ("/archjohn@gmail.com/[Google Mail].Starred"   . ?s)
         ("/NewsLetter" . ?w)
         ("/Drafts" . ?d)
         ("/archjohn@gmail.com/[Google Mail].Sent Mail" . ?m)
         ("/me@evgeniy.cc/Sent" . ?t)
         ))
(add-to-list 'mu4e-bookmarks
             (make-mu4e-bookmark
              :name  "Unread messages archjohn@gmail.com"
              :query "flag:unread AND NOT flag:trashed AND maildir:/archjohn@gmail.com/*"
              :key ?a) t)

(add-to-list 'mu4e-bookmarks
             (make-mu4e-bookmark
              :name  "Unread messages me@evgeniy.cc"
              :query "flag:unread AND NOT flag:trashed AND maildir:/me@evgeniy.cc/*"
              :key ?i) t)

(add-to-list 'mu4e-bookmarks
             (make-mu4e-bookmark
              :name "messages for me@evgeniy.cc"
              :query "to:me@evgeniy.cc"
              :key ?z) t)

(add-to-list 'mu4e-bookmarks
             (make-mu4e-bookmark
              :name "Today's messages me@evgeniy.cc"
              :query "date:today..now AND maildir:/me@evgeniy.cc/*"
              :key ?o) t)

(add-to-list 'mu4e-bookmarks
             (make-mu4e-bookmark
              :name  "Big messages"
              :query "size:5M..500M"
              :key ?b) t)

;; == Contexts ==
(setq mu4e-contexts
      `( ,(make-mu4e-context
           :name "Gmail"
           :enter-func (lambda () (mu4e-message "Entering Gmail context"))
           :leave-func (lambda () (mu4e-message "Leaving Gmail context"))
           ;; we match based on the contact-fields of the message
           :match-func (lambda (msg)
                         (when msg
                           (mu4e-message-contact-field-matches msg
                                                               :to "archjohn@gmail.com")))
           :vars '( ( user-mail-address    . "archjohn@gmail.com"  )
                    ( user-full-name    . "Evgeniy T" )
                    ( message-send-mail-function . smtpmail-send-it )
                    ( smtpmail-stream-type . starttls )
                    ( smtpmail-default-smtp-server . "smtp.gmail.com" )
                    ( smtpmail-smtp-server . "smtp.gmail.com" )
                    ( smtpmail-smtp-service . 587 )
                    ( mu4e-compose-signature .
                                             (concat "Cheers,\n"
                                                     "Best Regards\n"))))
         ,(make-mu4e-context
           :name "Zoho"
           :enter-func (lambda () (mu4e-message "Switch to the Zoho context"))
           ;; no leave-func
           ;; we match based on the contact-fields of the message
           ;; :match-func (lambda (msg)
           ;;       (when msg
           ;;         (mu4e-message-contact-field-matches msg
           ;;           :to "me@evgeniy.cc")))
           ;; we match based on the maildir of the message
           ;; this matches maildir /Arkham and its sub-directories
           :match-func (lambda (msg)
                         (when msg
                           (string-match-p "me@evgeniy.cc" (mu4e-message-field msg :maildir))
                           ))
           :vars
           '( ( user-mail-address     . "me@evgeniy.cc" )
              ( user-full-name    . "Evgeniy T" )
              ( message-send-mail-function . smtpmail-send-it )
              ( smtpmail-stream-type . starttls )
              ( smtpmail-default-smtp-server . "smtp.zoho.com" )
              ( smtpmail-smtp-server . "smtp.zoho.com" )
              ( smtpmail-smtp-service . 587 )
              ( mu4e-compose-signature  .
                                        (concat "Cheers,\n"
                                                "Best Regards\n"))
              ))))

;; start with the first (default) context;
;; default is to ask-if-none (ask when there's no context yet, and none match)
(setq mu4e-context-policy 'pick-first)

;; compose with the current context is no context matches;
;; default is to ask
(setq mu4e-compose-context-policy nil)

(add-hook 'text-mode-hook 'turn-on-flyspell)

;(add-hook 'prog-mode-hook 'flyspell-prog-mode)
;(remove-hook 'prog-mode-hook 'flyspell-prog-mode)

(defun cycle-languages ()
  "Changes the ispell dictionary to the first element in
ISPELL-LANGUAGES, and returns an interactive function that cycles
the languages in ISPELL-LANGUAGES when invoked."
  (lexical-let ((ispell-languages '#1=("ru_RU" "en_US" . #1#))) ;; those are for hunspell, for aspell: "american" "ru"
    (ispell-change-dictionary (car ispell-languages))
    (lambda ()
      (interactive)
      ;; Rotates the languages cycle and changes the ispell dictionary.
      (ispell-change-dictionary
       (car (setq ispell-languages (cdr ispell-languages)))))))

(defadvice turn-on-flyspell (before check nil activate)
  "Turns on flyspell only if a spell-checking tool is installed."
  (when (executable-find ispell-program-name)
    (local-set-key (kbd "C-c l") (cycle-languages))))

(defadvice flyspell-prog-mode (before check nil activate)
  "Turns on flyspell only if a spell-checking tool is installed."
  (when (executable-find ispell-program-name)
    (local-set-key (kbd "C-c l") (cycle-languages))))

;; (setq ispell-local-dictionary-alist
;;       '(("russian"
;;          "[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
;;          "[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
;;          "[-]"  nil ("-d" "ru_RU") nil utf-8)
;;         ("english"
;;          "[A-Za-z]" "[^A-Za-z]"
;;          "[']"  nil ("-d" "en_US") nil iso-8859-1)))

;; hunspell better than aspell
(setq ispell-really-aspell t
      ispell-really-hunspell nil)

;; path to hunspell
(setq ispell-program-name "aspell")
(setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))
(setq ispell-local-dictionary-alist nil)
(setq ispell-dictionary "en_US")
;; (setq ispell-program-name "/usr/local/bin/hunspell")

;; you could set `ispell-dictionary` instead but `ispell-local-dictionary' has higher priority
;;(setq ispell-dictionary "ru_RU")
;;(setq ispell-local-dictionary-alist '(("ru_RU" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "ru_RU,en_US") nil utf-8)))

;; dictionaries path
;; check ispell-hunspell-dict-paths-alist
(setq ispell-library-directory nil)

(add-hook 'auto-save-hook 'org-save-all-org-buffers)
(setq auto-save-interval 600)
(setq auto-save-timeout 120)

(setq org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-confirm-babel-evaluate nil
      org-edit-src-content-indentation 0)

(eval-after-load "org"
  '(progn
     (setcar (nthcdr 2 org-emphasis-regexp-components) " \t\n,")
     (custom-set-variables `(org-emphasis-alist ',org-emphasis-alist))))

;; hotkeys to open main org files
(defun go-to-inbox-org () "go to inbox.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/inbox.org"))
(defun go-to-todo-org () "go to todo.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/todo.org"))
(defun go-to-work-todo-org () "go to home_todo.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/01_work/work_todo.org"))
(defun go-to-home-todo-org () "go to work_todo.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/02_home/home_todo.org"))
(defun go-to-notes-org () "go to notes.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/notes.org"))
(defun go-to-blog-org () "go to blog.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/02_home/blog.org"))
(defun go-to-home-notes-org () "go to blog.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/02_home/notes.org"))
(defun go-to-home-parcelist-org () "go to parcelist.org" (interactive) (find-file "~/Dropbox/dev/10_org/agenda/02_home/parcelist.org"))

(global-set-key (kbd "C-c o i") 'go-to-inbox-org)
(global-set-key (kbd "C-c o t") 'go-to-todo-org)
(global-set-key (kbd "C-c o w t") 'go-to-work-todo-org)
(global-set-key (kbd "C-c o h t") 'go-to-home-todo-org)
(global-set-key (kbd "C-c o h n") 'go-to-home-notes-org)
(global-set-key (kbd "C-c o h p") 'go-to-home-parcelist-org)
(global-set-key (kbd "C-c o o") 'go-to-notes-org)
(global-set-key (kbd "C-c o h b") 'go-to-blog-org)

(let ((agenda-paths '("~/Dropbox/dev/10_org/agenda/"))
      (notes-path (expand-file-name "~/Dropbox/dev/10_org/agenda/notes.org")))

  (setq org-directory (expand-file-name "~/Dropbox/dev/10_org/"))

  ;; capture
  (setq org-default-notes-file notes-path)
  (setq org-capture-templates
        '(("t" "todo" entry (file+headline (lambda ()(concat org-directory "todo.org")) "ToDo")
           "* TODO %?\n %U\n")

          ("i" "inbox" entry (file+headline (lambda ()(concat org-directory "inbox.org")) "Random")
           "* %? :NOTE:\n%U\n")

          ("n" "note" entry (file+headline (lambda ()(concat org-directory "notes.org")) "Random")
           "* %? :NOTE:\n%U\n")

          ("b" "blog" entry (file+olp+datetree (lambda ()(concat org-directory "02_home/blog.org")))
           "* %?\nEntered on %U\n  %i\n")
          ))
  ;; agenda
  (setq org-agenda-files agenda-paths)
  (setq org-agenda-files (directory-files-recursively (car agenda-paths) "\\.org$")))



(setq org-icalendar-include-todo t)
;; mobile
;;(setq org-mobile-files "path/to/other/files")
;;     (setq org-mobile-inbox-for-pull notes-path)
;;     (setq org-mobile-directory "~/Dropbox/apps/strobe-MobileOrg/"))

(setq org-agenda-custom-commands
      '(("h" tags-todo "CATEGORY=\"home\"")
        ("w" tags-todo "CATEGORY=\"work\"")
        ("l" tags-todo "CATEGORY=\"all\"")
        ;; other custom agenda commands here
        ))

;; ditaa for org mode configuration (aka ANSI -> Diagrams)
(setq org-confirm-babel-evaluate nil)
(setq org-ditaa-jar-path "/usr/local/Cellar/ditaa/0.11.0/libexec/ditaa-0.11.0-standalone.jar")
(setq org-export-allow-BIND t)

(setq org-todo-keywords
      '((sequence "TODO" "INPROGRESS" "FEEDBACK" "VERIFY" "|" "DONE" "DELEGATED" "PAUSED")))

(setq org-columns-default-format "%25ITEM %PRIORITY %TODO %EFFORT %TAGS")

(setq org-agenda-overriding-columns-format "%25ITEM %PRIORITY %TODO %EFFORT %TAGS")

(setq yas-global-mode t)

;; fix some org-mode + yasnippet conflicts:
;;(defun yas/org-very-safe-expand ()
;;  (let ((yas-fallback-behavior 'return-nil)) (yas-expand)))

(add-hook 'org-mode-hook
          (lambda ()
            (setq before-save-hook nil)
            (make-variable-buffer-local 'yas/trigger-key)
            (setq yas/trigger-key [tab])
            ;;(add-to-list 'org-tab-first-hook 'yas/org-very-safe-expand)
            ;;(define-key yas/keymap [tab] 'yas/next-field)
            ))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   ;;(scala . t)
   (ditaa . t)
   (latex . t)))

(add-hook 'org-present-mode-hook
          (lambda ()
            (org-present-big)
            (org-display-inline-images)))

(add-hook 'org-present-mode-quit-hook
          (lambda ()
            (org-present-small)
            (org-remove-inline-images)))

(add-hook 'epresent-start-presentation-hook
          (lambda ()
            (olivetti-mode t)))

(add-hook 'epresent-stop-presentation-hook
          (lambda ()
            (olivetti-mode -1)))

(set-face-attribute 'outline-1 nil :background nil :height 1.0 :bold t)
(set-face-attribute 'outline-2 nil :background nil :height 1.0 :bold nil)
(set-face-attribute 'outline-3 nil :background nil :height 1.0 :bold nil)
(set-face-attribute 'outline-4 nil :background nil :height 1.0 :bold nil)
(set-face-attribute 'outline-5 nil :background nil :height 1.0 :bold nil)
(set-face-attribute 'outline-6 nil :background nil :height 1.0 :bold nil)
(set-face-attribute 'outline-7 nil :background nil :height 1.0 :bold nil)
(set-face-attribute 'outline-8 nil :background nil :height 1.0 :bold nil)

;; default options for all output formats
(setq org-pandoc-options '((standalone . t)))
;; cancel above settings only for 'docx' format
(setq org-pandoc-options-for-docx '((standalone . nil)))
;; special settings for beamer-pdf and latex-pdf exporters
(setq org-pandoc-options-for-beamer-pdf '((pdf-engine . "xelatex"))) ;; "xelatex"
(setq org-pandoc-options-for-latex-pdf '((pdf-engine . "xelatex")))

;;(add-to-list 'org-beamer-environments-default
;;                   '("codeframe" "c" "\\begin{frame}%a\\begin{codeframe}{%h}" "\\end{codeframe\\end{frame}"))

;;(setq org-latex-prefer-user-labels t)

(defun hightlighted-latex-export-src-blocks (text backend info)
  "Export src blocks as listings env."
  (when (org-export-derived-backend-p backend 'latex)
    (with-temp-buffer
      (insert text)
      ;; replace verbatim env by listings
      (goto-char (point-min))
      (replace-string "\\begin{verbatim}" "\\begin{lstlisting}")
      (replace-string "\\end{verbatim}" "\\end{lstlisting}")
      (buffer-substring-no-properties (point-min) (point-max)))))

(add-to-list 'org-export-filter-src-block-functions
             'hightlighted-latex-export-src-blocks)

(add-to-list 'org-latex-classes
             '("article"
               "\\documentclass{article}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes
             '("book"
               "\\documentclass{book}"
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
             )

(add-to-list 'org-latex-classes
             '("koma-article"
               "\\documentclass{scrartcl}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(defun cycle-spacing-delete-newlines ()
  "Removes whitespace before and after the point."
  (interactive)
  (if (version< emacs-version "24.4")
      (just-one-space -1)
    (cycle-spacing -1)))

(defun jump-to-symbol-internal (&optional backwardp)
  "Jumps to the next symbol near the point if such a symbol
exists. If BACKWARDP is non-nil it jumps backward."
  (let* ((point (point))
         (bounds (find-tag-default-bounds))
         (beg (car bounds)) (end (cdr bounds))
         (str (isearch-symbol-regexp (find-tag-default)))
         (search (if backwardp 'search-backward-regexp
                   'search-forward-regexp)))
    (goto-char (if backwardp beg end))
    (funcall search str nil t)
    (cond ((<= beg (point) end) (goto-char point))
          (backwardp (forward-char (- point beg)))
          (t  (backward-char (- end point))))))

(defun jump-to-previous-like-this ()
  "Jumps to the previous occurrence of the symbol at point."
  (interactive)
  (jump-to-symbol-internal t))

(defun jump-to-next-like-this ()
  "Jumps to the next occurrence of the symbol at point."
  (interactive)
  (jump-to-symbol-internal))

(defun kill-this-buffer-unless-scratch ()
  "Works like `kill-this-buffer' unless the current buffer is the
*scratch* buffer. In witch case the buffer content is deleted and
the buffer is buried."
  (interactive)
  (if (not (string= (buffer-name) "*scratch*"))
      (kill-this-buffer)
    (delete-region (point-min) (point-max))
    (switch-to-buffer (other-buffer))
    (bury-buffer "*scratch*")))

(defun duplicate-thing (comment)
  "Duplicates the current line, or the region if active. If an argument is
given, the duplicated region will be commented out."
  (interactive "P")
  (save-excursion
    (let ((start (if (region-active-p) (region-beginning) (point-at-bol)))
          (end   (if (region-active-p) (region-end) (point-at-eol))))
      (goto-char end)
      (unless (region-active-p)
        (newline))
      (insert (buffer-substring start end))
      (when comment (comment-region start end)))))

(defun tidy ()
  "Ident, untabify and unwhitespacify current buffer, or region if active."
  (interactive)
  (let ((beg (if (region-active-p) (region-beginning) (point-min)))
        (end (if (region-active-p) (region-end) (point-max))))
    (indent-region beg end)
    (whitespace-cleanup)
    (untabify beg (if (< end (point-max)) end (point-max)))))

(defun org-sync-pdf ()
  (interactive)
  (let ((headline (nth 4 (org-heading-components)))
        (pdf (concat (file-name-base (buffer-name)) ".pdf")))
    (when (file-exists-p pdf)
      (find-file-other-window pdf)
      (pdf-links-action-perform
       (cl-find headline (pdf-info-outline pdf)
                :key (lambda (alist) (cdr (assoc 'title alist)))
                :test 'string-equal)))))

(defun split-root-window (size direction)
  (split-window (frame-root-window)
                (and size (prefix-numeric-value size))
                direction))

(defun split-root-window-below (&optional size)
  (interactive "P")
  (split-root-window size 'below))

(defun my-split-root-window-right (&optional size)
  (interactive "P")
  (split-root-window size 'right))

;; from https://gist.github.com/3402786
(defun toggle-maximize-buffer ()
  "Maximize buffer"
  (interactive)
  (if (and (= 1 (length (window-list)))
           (assoc ?_ register-alist))
      (jump-to-register ?_)
    (progn
      (window-configuration-to-register ?_)
      (delete-other-windows))))

;; dublicate line
(defun duplicate-line (arg)
  "Duplicate current line, leaving point in lower line."
  (interactive "*p")

  ;; save the point for undo
  (setq buffer-undo-list (cons (point) buffer-undo-list))

  ;; local variables for start and end of line
  (let ((bol (save-excursion (beginning-of-line) (point)))
        eol)
    (save-excursion
      ;; don't use forward-line for this, because you would have
      ;; to check whether you are at the end of the buffer
      (end-of-line)
      (setq eol (point))

      ;; store the line and disable the recording of undo information
      (let ((line (buffer-substring bol eol))
            (buffer-undo-list t)
            (count arg))
        ;; insert the line arg times
        (while (> count 0)
          (newline)         ;; because there is no newline in 'line'
          (insert line)
          (setq count (1- count)))
        )
      ;; create the undo information
      (setq buffer-undo-list (cons (cons eol (point)) buffer-undo-list)))
    ) ; end-of-let

  ;; put the point in the lowest line and return
  (next-line arg))

(defun comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)))

(defun fix-buffer-window ()
  "Fix buffer to specifix window pane.
   So whenever a new file is opened, the file in that window pane remains on top of the buf"
  (interactive)
  (set-window-dedicated-p (selected-window) t)
  )

(defun unfix-buffer-window ()
  (interactive)
  (set-window-dedicated-p (selected-window) nil)
  )

(defun comment-region-wrap ()
  "wrapp region of text to multiline comments /* ... */"
  (interactive)
  (progn
    (save-excursion
      (goto-char (region-beginning))
      (insert "/*"))
    (save-excursion
      (goto-char (region-end))
      (insert "\n*/"))))

(defadvice eval-last-sexp (around replace-sexp (arg) activate)
  "Replace sexp when called with a prefix argument."
  (if arg
      (let ((pos (point)))
        ad-do-it
        (goto-char pos)
        (backward-kill-sexp)
        (forward-sexp))
    ad-do-it))

(defadvice load-theme
    (before disable-before-load (theme &optional no-confirm no-enable) activate)
  (progn
    (mapc 'disable-theme custom-enabled-themes)
    (let ((private-file (concat user-emacs-directory "private.el")))
      (when (file-exists-p private-file)
        (load-file private-file)))))

(autoload 'csharp-mode "csharp-mode" "Major mode for editing C# code." t)
;; (defun l-csharp-mode-hook ()
;; enable the stuff you want for C# here
;; (electric-pair-local-mode 1) ;; Emacs 25
;;  )

;;(add-hook 'csharp-mode-hook 'l-csharp-mode-hook)

(lexical-let* ((default (face-attribute 'default :height))
               (size default))

  (defun global-scale-default ()
    (interactive)
    (setq size default)
    (global-scale-internal size))

  (defun global-scale-up ()
    (interactive)
    (global-scale-internal (incf size 20)))

  (defun global-scale-down ()
    (interactive)
    (global-scale-internal (decf size 20)))

  (defun global-scale-internal (arg)
    (set-face-attribute 'default (selected-frame) :height arg)
    (set-temporary-overlay-map
     (let ((map (make-sparse-keymap)))
       (define-key map (kbd "C-=") 'global-scale-up)
       (define-key map (kbd "C-+") 'global-scale-up)
       (define-key map (kbd "C--") 'global-scale-down)
       (define-key map (kbd "C-0") 'global-scale-default) map))))

;;  (progn (centaur-tabs-mode t)
;;          (centaur-tabs-change-fonts "Iosevka SS08" 120)
;;          (set-face-attribute 'centaur-tabs-active-bar-face nil :background "#2a6491")
;;          (set-face-attribute 'tab-line nil :background "gray15"))
;;
;;   (defun centaur-tabs-buffer-groups ()
;;     "`centaur-tabs-buffer-groups' control buffers' group rules.
;;
;;              Group centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode' `dired-mode' `org-mode' `magit-mode'.
;;              All buffer name start with * will group to \"Emacs\".
;;              Other buffer group by `centaur-tabs-get-group-name' with project name."
;;     (list
;;      (cond
;;       ((derived-mode-p 'term-mode) "Terminal")
;;       ((or (string-equal "*" (substring (buffer-name) 0 1))
;;            (memq major-mode '(magit-process-mode
;;                               magit-status-mode
;;                               magit-diff-mode
;;                               magit-log-mode
;;                               magit-file-mode
;;                               magit-blob-mode
;;                               magit-blame-mode
;;                               dired-sidebar-mode
;;                               ))) "Emacs")
;;       ((or (derived-mode-p 'fundamental-mode)
;;            (memq major-mode '(fundamental-mode))) "Editing")
;;       ((derived-mode-p 'dired-mode) "Dired")
;;       ((memq major-mode '(helpful-mode
;;                           help-mode)) "Help")
;;       ((memq major-mode '(org-mode
;;                           org-agenda-clockreport-mode
;;                           org-src-mode
;;                           org-agenda-mode
;;                           org-beamer-mode
;;                           org-indent-mode
;;                           org-bullets-mode
;;                           org-cdlatex-mode
;;                           org-agenda-log-mode
;;                           diary-mode)) "OrgMode")
;;       ;; group by project
;;       (t (centaur-tabs-get-group-name (current-buffer))))))
;;   ;;(t "Ungrouped"))))
;;
;;   (defun centaur-tabs-hide-tab (x)
;;     (let ((name (format "%s" x)))
;;       (or
;;        (string-prefix-p "*lsp" name)
;;        (string-prefix-p "*lsp-ui" name)
;;        (string-prefix-p "*which-key*" name)
;;        (string-prefix-p "*helm" name)
;;        (string-prefix-p "*epc" name)
;;        (string-prefix-p ":" name)
;;        (string-prefix-p "*Compile-Log*" name)
;;        (and (string-prefix-p "magit" name)
;;             (not (file-name-extension name)))
;;        )))
;;    (add-hook 'dired-sidebar-mode-hook 'centaur-tabs-local-mode)
;;    (add-hook 'which-key-mode-hook 'centaur-tabs-local-mode)
;;    (add-hook 'neotree-mode-hook 'centaur-tabs-local-mode)
;;    (add-hook 'lsp-ui-doc-frame-mode-hook 'centaur-tabs-local-mode)
;;
;;   ;;
;;   ;;    ‘default: (Already described)
;;   ;;    ‘tabs: Cycle through visible tabs (that is, the tabs in the current group)
;;   ;;    ‘groups: Navigate through tab groups only
;;   (setq centaur-tabs-cycle-scope 'tabs)
;;
;;   (setq centaur-tabs-set-bar 'over)
;;
;;   (setq centaur-tabs-set-modified-marker t)
;;   (setq centaur-tabs-modified-marker "*")
;;
;;   (setq centaur-tabs-height 26)
;;   ;;(setq centaur-tabs-bar-height 50)
;;   (setq centaur-tabs-style "bar")
;;   (setq centaur-tabs-icon-scale-factor 0.6)
;;   (setq centaur-tabs-set-icons nil)
;;   (setq centaur-tabs-gray-out-icons 'buffer)
;;
;;   ;;(add-hook 'which-key-mode-hook 'centaur-tabs-local-mode)
;;
;;   (global-set-key (kbd "C-<prior>")  'centaur-tabs-backward)
;;   (global-set-key (kbd "C-<next>") 'centaur-tabs-forward)
;;   (global-set-key (kbd "<header-line> <wheel-left>")  'centaur-tabs-backward)
;;   (global-set-key (kbd "<header-line> <wheel-right>") 'centaur-tabs-forward)
;;
                                        ;(centaur-tabs-force-update)

;;(which-key-posframe-mode nil)
;;(setq which-key-posframe-poshandler 'posframe-poshandler-frame-bottom-left-corner)
;;(setq which-key-posframe-poshandler 'posframe-poshandler-window-bottom-left-corner)
;;(setq which-key-posframe-poshandler 'posframe-poshandler-point-bottom-left-corner)

;;(desktop-save-mode t)

(require 'sql)
(setq sql-port 3306)
(setq sql-mysql-login-params (append sql-mysql-login-params '(port)))

(defun setup-tide-mode ()
  "Setup function for tide."
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (company-mode +1))

(setq company-tooltip-align-annotations t)

(add-hook 'js-mode-hook #'setup-tide-mode)

(setq mhtml-tag-relative-indent t)

(setq olivetti-body-width 120)

(require 'dired)

;; dired default target dir (for mc like operations)
(setq dired-dwim-target t)
(put 'dired-find-alternate-file 'disabled nil)

(when (eq system-type 'darwin)
  (setq dired-use-ls-dired nil))

;;:bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
(cond ((eq system-type 'darwin)
       (setq dired-sidebar-theme 'nerd))
      ((eq system-type 'windows-nt)
       (setq dired-sidebar-theme 'nerd))
      (:default
       (setq dired-sidebar-theme 'nerd)))
(setq dired-sidebar-use-term-integration t)
(setq dired-sidebar-use-custom-font t)

(setq dired-sidebar-width 35)
(setq dired-sidebar-should-follow-file t)
(setq dired-sidebar-follow-file-at-point-on-toggle-open t)
(setq dired-sidebar-use-evil-integration nil)
(setq dired-sidebar-use-term-integration nil)
(setq dired-subtree-cycle-depth 16)

(defun unlock-window-size ()
  "unlock sidebar width"
  (interactive)
  (setq window-size-fixed nil))

;(add-hook 'dired-mode-hook 'dired-collapse-mode)

(defun mydired-sort ()
  "Sort dired listings with directories first."
  (save-excursion
    (let (buffer-read-only)
      (forward-line 2) ;; beyond dir. header
      (sort-regexp-fields t "^.*$" "[ ]*." (point) (point-max)))
    (set-buffer-modified-p nil)))

(defadvice dired-readin
    (after dired-after-updating-hook first () activate)
  "Sort dired listings with directories first before adding marks."
  (mydired-sort))

;;(setq neo-theme 'arrow)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(setq neo-show-updir-line t)
(setq neo-smart-open t)
(setq neo-show-hidden-files t)
(setq neo-window-width 35)
(setq neo-window-fixed-size nil)

(defun neotree-project-dir-toggle ()
  "Open NeoTree using the project root, using find-file-in-project, or the current buffer directory."
  (interactive)
  (let ((project-dir
         (ignore-errors
         ;;; Pick one: projectile or find-file-in-project
                                        ; (projectile-project-root)
           (ffip-project-root)
           ))
        (file-name (buffer-file-name))
        (neo-smart-open t))
    (if (and (fboundp 'neo-global--window-exists-p)
             (neo-global--window-exists-p))
        (neotree-hide)
      (progn
        (neotree-show)
        (if project-dir
            (neotree-dir project-dir))
        (if file-name
            (neotree-projectile-action)
          (neotree-find file-name))))))

;; will use TUI string icons even when running in a GUI.
(setq treemacs-no-png-images t)
(setq treemacs-follow-mode t)
(treemacs-git-mode 'deferred)

(require 'sr-speedbar)
(setq speedbar-use-images nil)
(setq sr-speedbar-right-side t)

(custom-set-faces
 '(speedbar-button-face ((t (:foreground "selectedTextBackgroundColor"))))
 '(speedbar-file-face ((t (:foreground "gray70"))))
 '(speedbar-separator-face ((t (:background "gray20" :foreground "white")))))

(setenv "SHELL" "/bin/bash")

(lexical-let ((last-shell ""))
  (defun toggle-shell ()
    (interactive)
    (cond ((string-match-p "^\\*shell<[1-9][0-9]*>\\*$" (buffer-name))
           (goto-non-shell-buffer))
          ((get-buffer last-shell) (switch-to-buffer last-shell))
          (t (ansi-term (setq last-shell "*ansi-term*<1>*")))))

  (defun switch-shell (n)
    (let ((buffer-name (format "*ansi-term*<%d>*" n)))
      (setq last-shell buffer-name)
      (cond ((get-buffer buffer-name)
             (switch-to-buffer buffer-name))
            (t (ansi-term (getenv "SHELL") buffer-name)
               (rename-buffer buffer-name)))))

  (defun goto-non-shell-buffer ()
    (let* ((r "^\\*shell<[1-9][0-9]*>\\*$")
           (shell-buffer-p (lambda (b) (string-match-p r (buffer-name b))))
           (non-shells (cl-remove-if shell-buffer-p (buffer-list))))
      (when non-shells
        (switch-to-buffer (first non-shells))))))

(defadvice shell (after kill-with-no-query nil activate)
  (set-process-query-on-exit-flag (get-buffer-process ad-return-value) nil))

(defun clear-comint ()
  "Runs `comint-truncate-buffer' with the
`comint-buffer-maximum-size' set to zero."
  (interactive)
  (let ((comint-buffer-maximum-size 0))
    (comint-truncate-buffer)))

(add-hook 'comint-mode-hook (lambda () (local-set-key (kbd "C-l") 'clear-comint)))

(defun open-sans-buffer-face-mode-fixed ()
  "Sets OpenSans font in current buffer"
  (interactive)
  (setq buffer-face-mode-face '(:family "Open Sans"))
  (buffer-face-mode))

(add-hook 'w3m-mode-hook 'open-sans-buffer-face-mode-fixed)

;; terminal
(setq multi-term-program "/bin/bash")
'(shell-file-name "/bin/bash")

;; bash completion
(autoload 'bash-completion-dynamic-complete
  "bash-completion"
  "BASH completion hook")
(add-hook 'shell-dynamic-complete-functions
          'bash-completion-dynamic-complete)
(bash-completion-setup)

;; terminal mouse pointer support
(require 'mwheel)
(require 'mouse)
(xterm-mouse-mode t)
(mouse-wheel-mode t)
(global-set-key [mouse-4] 'previous-line)
(global-set-key [mouse-5] 'next-line)

;;(xterm-mouse-mode t)
;;(setq mouse-sel-mode t)

;; terminal mouse scroll ;;
;;(defun track-mouse (e))

;; (setq mouse-wheel-follow-mouse 't)
;;
;; (defvar alternating-scroll-down-next t)
;; (defvar alternating-scroll-up-next t)
;;
;; (defun alternating-scroll-down-line ()
;;   (interactive "@")
;;   (when alternating-scroll-down-next
;;                                         ;(run-hook-with-args 'window-scroll-functions )
;;     (scroll-down-line))
;;   (setq alternating-scroll-down-next (not alternating-scroll-down-next)))
;;
;; (defun alternating-scroll-up-line ()
;;   (interactive "@")
;;   (when alternating-scroll-up-next
;;                                         ;(run-hook-with-args 'window-scroll-functions)
;;     (scroll-up-line))
;;   (setq alternating-scroll-up-next (not alternating-scroll-up-next)))
;;
;; (global-set-key (kbd "<mouse-4>") 'alternating-scroll-down-line)
;; (global-set-key (kbd "<mouse-5>") 'alternating-scroll-up-line)
;; end of terminal  mouse scroll ;;

(persistent-scratch-setup-default) ;; enable autosave and restore the last saved state on Emacs start
(setq persistent-scratch-autosave-mode t) ;; Autosave

(dolist (mode '(;cider-repl-mode
                                        ;clojure-mode
                ielm-mode
                geiser-repl-mode
                                        ;slime-repl-mode
                                        ;lisp-mode
                emacs-lisp-mode
                                        ;lisp-interaction-mode
                                        ;scheme-mode
                ))
  ;; add paredit-mode to all mode-hooks
  (add-hook (intern (concat (symbol-name mode) "-hook")) 'paredit-mode))

(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)

(defun c-setup ()
  (local-set-key (kbd "C-c C-c") 'compile))

(add-hook 'c-mode-common-hook 'c-setup)

(define-abbrev-table 'java-mode-abbrev-table
  '(("psv" "public static void main(String[] args) {" nil 0)
    ("sopl" "System.out.println" nil 0)
    ("sop" "System.out.printf" nil 0)))

(defun java-setup ()
  (abbrev-mode t)
  (setq-local compile-command (concat "javac " (buffer-name))))

(add-hook 'java-mode-hook 'java-setup)

;; (use-package ensime
;;   :ensure t
;;   :pin melpa-stable)
;;
;; (setq ensime-startup-notification nil)
;; (setq ensime-startup-snapshot-notification nil)
;;
;; (set-face-attribute 'ensime-implicit-highlight nil :underline "#343434")
;;
;;
;; (setq scala-indent:align-parameters nil)
;;
;; (defun ensime-edit-definition-with-fallback ()
;;   "Variant of `ensime-edit-definition' with ctags if ENSIME is not available."
;;   (interactive)
;;   (unless (and (ensime-connection-or-nil)
;;                (ensime-edit-definition nil))
;;     (projectile-find-tag)))

(defun generate-scala-etags ()
  (interactive)
  (let* ((project-root (projectile-project-root))
         (tags-exclude (projectile-tags-exclude-patterns))
         (default-directory project-root)
         (tags-file (expand-file-name projectile-tags-file-name)))
    (command (format "sctags -f TAGS -e -R \"%s\" %s" tags-file tags-exclude))
    shell-output exit-code))

;; Enable defer and ensure by default for use-package
    ;; Keep auto-save/backup files separate from source code:  https://github.com/scalameta/metals/issues/1027
    (setq use-package-always-defer t
          use-package-always-ensure t
          backup-directory-alist `((".*" . ,temporary-file-directory))
          auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

    ;; Enable scala-mode and sbt-mode
    (use-package scala-mode
      :mode "\\.s\\(cala\\|bt\\)$")

    (use-package sbt-mode
      :commands sbt-start sbt-command
      :config
      ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
      ;; allows using SPACE when in the minibuffer
      (substitute-key-definition
       'minibuffer-complete-word
       'self-insert-command
       minibuffer-local-completion-map)
      ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
      (setq sbt:program-options '("-Dsbt.supershell=false")))

    (add-hook 'scala-mode-hook 'flycheck-mode)

    ;; Enable nice rendering of diagnostics like compile errors.
    (use-package flycheck
      :init (global-flycheck-mode))

    ;; WARN: if somentig doesn't works check that lsp-mode compiled by emacs well
    (use-package lsp-mode
      ;; Optional - enable lsp-mode automatically in scala files
      :hook (scala-mode . lsp)
      :hook (lsp-mode . lsp-lens-mode)
      :config (setq lsp-prefer-flymake nil))

    (add-hook 'scala-mode-hook #'lsp-deferred)

    ;; Add metals backend for lsp-mode
    (use-package lsp-metals
      :config (setq lsp-metals-treeview-show-when-views-received nil))

    ;; Add metals backend for lsp-mode
    ;;(use-package lsp-metals)

    (use-package lsp-ui)

    ;; Add company-lsp backend for metals
    (use-package company-lsp)

    ;; Use the Debug Adapter Protocol for running tests and debugging
    (use-package posframe
      ;; Posframe is a pop-up tool that must be manually installed for dap-mode
      )

;;   (use-package dap-mode
;;      :hook
;;      (lsp-mode . dap-mode)
;;      (lsp-mode . dap-ui-mode)
;;      )

    ;; Use the Tree View Protocol for viewing the project structure and triggering compilation
    ;; (use-package lsp-treemacs
    ;;   :config
    ;;   (lsp-metals-treeview-enable t)
    ;;   (setq lsp-metals-treeview-show-when-views-received t)
    ;;   )

    ;; customize doc frame
    (setq lsp-ui-doc-position 'at-point) ; top ; bottom
    (setq lsp-ui-doc-alignment 'window) ; frame window

    ;; disable sideviw hover
    ;;(setq lsp-ui-sideline-show-hover nil)

    (setq lsp-metals-treeview-show-when-views-received nil)

(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

(defun insert-markdown-inline-math-block ()
  "Inserts an empty math-block if no region is active, otherwise wrap a
math-block around the region."
  (interactive)
  (let* ((beg (region-beginning))
         (end (region-end))
         (body (if (region-active-p) (buffer-substring beg end) "")))
    (when (region-active-p)
      (delete-region beg end))
    (insert (concat "$math$ " body " $/math$"))
    (search-backward " $/math$")))

;;(add-hook 'markdown-mode-hook
;;          (lambda ()
;;            (auto-fill-mode 0)
;;            (visual-line-mode 1)
;;            ;; (ispell-change-dictionary "norsk")
;;            (local-set-key (kbd "C-c b") 'insert-markdown-inline-math-block)) t)

(defun fira-code-buffer-face-mode-fixed ()
  "Sets Fira Code font in current buffer"
  (interactive)
  (setq buffer-face-mode-face '(:family "Fira Mono Bold"))
  (buffer-face-mode))

(defun roboto-medium-buffer-face-mode-fixed ()
  "Sets Roboto Medium font in current buffer"
  (interactive)
  (setq buffer-face-mode-face '(:family "Roboto"))
  (buffer-face-mode))

;;(add-hook 'markdown-mode-hook 'fira-code-buffer-face-mode-fixed)

(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
(add-hook 'haskell-mode-hook
          (lambda ()
            (set (make-local-variable 'company-backends)
                 (append '((company-capf company-dabbrev-code))
                         company-backends))))
;;(add-hook 'haskell-mode-hook 'intero-mode)
(intero-global-mode t)
(add-hook 'haskell-mode-hook 'haskell-decl-scan-mode)
(setq intero-blacklist '("/"))
(setq intero-whitelist nil)

(require 'rust-mode)

;; Indentation
(add-hook 'rust-mode-hook
          (lambda () (setq indent-tabs-mode nil)))

;; Code formatting
(setq rust-format-on-save t)

;; Running / testing / compiling code
(define-key rust-mode-map (kbd "C-c C-c") 'rust-run)

;; flycheck
(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(defvar custom-bindings-map (make-keymap)
  "A keymap for custom bindings.")

;; Recode shortcuts in russian-layout
(defun reverse-input-method (input-method)
  "Build the reverse mapping of single letters from INPUT-METHOD."
  (interactive
   (list (read-input-method-name "Use input method (default current): ")))
  (if (and input-method (symbolp input-method))
      (setq input-method (symbol-name input-method)))
  (let ((current current-input-method)
        (modifiers '(nil (control) (meta) (control meta))))
    (when input-method
      (activate-input-method input-method))
    (when (and current-input-method quail-keyboard-layout)
      (dolist (map (cdr (quail-map)))
        (let* ((to (car map))
               (from (quail-get-translation
                      (cadr map) (char-to-string to) 1)))
          (when (and (characterp from) (characterp to))
            (dolist (mod modifiers)
              (define-key local-function-key-map
                (vector (append mod (list from)))
                (vector (append mod (list to)))))))))
    (when input-method
      (activate-input-method current))))

(defadvice read-passwd (around my-read-passwd act)
  (let ((local-function-key-map nil))
    ad-do-it))

(reverse-input-method 'russian-computer)

;;(define-key custom-bindings-map (kbd "C-M-t") 'centaur-tabs-local-mode)

;;  (defmacro def-pairs (pairs)
;;    "Define functions for pairing. PAIRS is an alist of (NAME . STRING)
;;  conses, where NAME is the function name that will be created and
;;  STRING is a single-character string that marks the opening character.
;;
;;    (def-pairs ((paren . \"(\")
;;                (bracket . \"[\"))
;;
;;  defines the functions WRAP-WITH-PAREN and WRAP-WITH-BRACKET,
;;  respectively."
;;    `(progn
;;       ,@(loop for (key . val) in pairs
;;               collect
;;               `(defun ,(read (concat
;;                               "wrap-with-"
;;                               (prin1-to-string key)
;;                               "s"))
;;                    (&optional arg)
;;                  (interactive "p")
;;                  (sp-wrap-with-pair ,val)))))
;;
;;  (def-pairs ((paren . "(")
;;              (bracket . "[")
;;              (brace . "{")
;;              (single-quote . "'")
;;              (double-quote . "\"")
;;              (back-quote . "`")))


(define-key custom-bindings-map (kbd "C-M-a")                       'sp-beginning-of-sexp)
(define-key custom-bindings-map (kbd "C-M-e")                       'sp-end-of-sexp)
;;(define-key custom-bindings-map (kbd ("C-<down>")                 'sp-down-sexp)
;;(define-key custom-bindings-map (kbd ("C-<up>")                   'sp-up-sexp)
(define-key custom-bindings-map (kbd "M-<down>")                    'sp-backward-down-sexp)
(define-key custom-bindings-map (kbd "M-<up>")                      'sp-backward-up-sexp)
(define-key custom-bindings-map (kbd "C-M-f")                       'sp-forward-sexp)
(define-key custom-bindings-map (kbd "C-M-b")                       'sp-backward-sexp)
(define-key custom-bindings-map (kbd "C-M-n")                       'sp-next-sexp)
(define-key custom-bindings-map (kbd "C-M-p")                       'sp-previous-sexp)
(define-key custom-bindings-map (kbd "C-S-f")                       'sp-forward-symbol)
(define-key custom-bindings-map (kbd "C-S-b")                       'sp-backward-symbol)
;;(define-key custom-bindings-map (kbd "C-<right>")                   'sp-forward-slurp-sexp)
;;(define-key custom-bindings-map (kbd "M-<right>")                   'sp-forward-barf-sexp)
;;(define-key custom-bindings-map (kbd "C-<left>")                    'sp-backward-slurp-sexp)
;;(define-key custom-bindings-map (kbd "M-<left>")                    'sp-backward-barf-sexp)
                                        ;(define-key custom-bindings-map (kbd "C-M-t")                       'sp-transpose-sexp)
(define-key custom-bindings-map (kbd "C-M-k")                       'sp-kill-sexp)
(define-key custom-bindings-map (kbd "C-k")                         'sp-kill-hybrid-sexp)
(define-key custom-bindings-map (kbd "M-k")                         'sp-backward-kill-sexp)
(define-key custom-bindings-map (kbd "C-M-w")                       'sp-copy-sexp)
(define-key custom-bindings-map (kbd "C-M-d")                       'delete-sexp)
(define-key custom-bindings-map (kbd "M-<backspace>")               'backward-kill-word)
(define-key custom-bindings-map (kbd "C-<backspace>")               'sp-backward-kill-word)
;;(define-key custom-bindings-map (kbd [remap sp-backward-kill-word]) 'backward-kill-word)
;;(define-key custom-bindings-map (kbd ("M-[")                       'sp-backward-unwrap-sexp)
;;(define-key custom-bindings-map (kbd ("M-]")                       'sp-unwrap-sexp)
(define-key custom-bindings-map (kbd "C-x C-t")                     'sp-transpose-hybrid-sexp)
;;(define-key custom-bindings-map (kbd "C-c (")                       'wrap-with-parens)
;;(define-key custom-bindings-map (kbd "C-c [")                       'wrap-with-brackets)
;;(define-key custom-bindings-map (kbd "C-c {")                       'wrap-with-braces)
;;(define-key custom-bindings-map (kbd "C-c '")                       'wrap-with-single-quotes)
;;(define-key custom-bindings-map (kbd "C-c \"")                      'wrap-with-double-quotes)
;;(define-key custom-bindings-map (kbd "C-c _")                       'wrap-with-underscores)
;;(define-key custom-bindings-map (kbd "C-c `")                       'wrap-with-back-quotes)

(define-key custom-bindings-map (kbd "s-N") 'new-buffer)

;;(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
;;(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
;;(global-set-key (kbd "S-C-<down>") 'shrink-window)
;;(global-set-key (kbd "S-C-<up>") 'enlarge-window)

;;(global-set-key (kbd "S-C-<left>") nil)
;;(global-set-key (kbd "S-C-<right>") nil)
;;(global-set-key (kbd "S-C-<down>") nil)
;;(global-set-key (kbd "S-C-<up>") nil)
;;(define-key custom-bindings-map (kbd "S-C-<left>")  nil)
;;(define-key custom-bindings-map (kbd "S-C-<right>") nil)
;;(define-key custom-bindings-map (kbd "S-C-<down>")  nil)
;;(define-key custom-bindings-map (kbd "S-C-<up>")    nil)

;;(define-key custom-bindings-map (kbd "S-C-<left>") 'shrink-window-horizontally)
;;(define-key custom-bindings-map (kbd "S-C-<right>") 'enlarge-window-horizontally)
;;(define-key custom-bindings-map (kbd "S-C-<down>") 'shrink-window)
;;(define-key custom-bindings-map (kbd "S-C-<up>") 'enlarge-window)

;; ergonomic way of moving between buffers
(global-set-key (kbd "M-=") 'other-window)
(global-set-key (kbd "C-M-]") 'next-buffer)
(global-set-key (kbd "C-M-[") 'previous-buffer)

(global-set-key (kbd "ESC ESC") nil)
;;(global-set-key (kbd "M-<right>") nil)
                                        ;(global-set-key (kbd "M-s-<left>") 'pop-global-mark)
;;(global-set-key (kbd "ESC <left>") 'pop-global-mark)

(defun marker-is-point-p (marker)
  "test if marker is current point"
  (and (eq (marker-buffer marker) (current-buffer))
       (= (marker-position marker) (point))))

(defun push-mark-maybe ()
  "push mark onto `global-mark-ring' if mark head or tail is not current location"
  (if (not global-mark-ring) (error "global-mark-ring empty")
    (unless (or (marker-is-point-p (car global-mark-ring))
                (marker-is-point-p (car (reverse global-mark-ring))))
      (push-mark))))


(defun backward-global-mark ()
  "use `pop-global-mark', pushing current point if not on ring."
  (interactive)
  (push-mark-maybe)
  (when (marker-is-point-p (car global-mark-ring))
    (call-interactively 'pop-global-mark))
  (call-interactively 'pop-global-mark))

(defun forward-global-mark ()
  "hack `pop-global-mark' to go in reverse, pushing current point if not on ring."
  (interactive)
  (push-mark-maybe)
  (setq global-mark-ring (nreverse global-mark-ring))
  (when (marker-is-point-p (car global-mark-ring))
    (call-interactively 'pop-global-mark))
  (call-interactively 'pop-global-mark)
  (setq global-mark-ring (nreverse global-mark-ring)))

;;(global-set-key (kbd "ESC <down>") nil)
;;(global-set-key (kbd "M-<down>") 'backward-global-mark)
;;(global-set-key (kbd "M-<up>") 'forward-global-mark)
;; (global-set-key (kbd "ESC <down>") 'backward-global-mark)
;; (global-set-key (kbd "ESC <up>") 'forward-global-mark)

(define-key custom-bindings-map (kbd "C-c D") 'define-word-at-point)

(define-key custom-bindings-map (kbd "C->")  'er/expand-region)
(define-key custom-bindings-map (kbd "C-<")  'er/contract-region)

(define-key custom-bindings-map (kbd "C-c e")  'mc/edit-lines)
(define-key custom-bindings-map (kbd "C-c a")  'mc/mark-all-like-this)
(define-key custom-bindings-map (kbd "C-c n")  'mc/mark-next-like-this)
(define-key custom-bindings-map (kbd "S-s-<mouse-1>")  'mc/add-cursor-on-click)
;;(global-set-key (kbd "M-m c") 'mc/edit-lines)
(global-set-key (kbd "C-c >") 'mc/mark-next-like-this)
(global-set-key (kbd "C-c <") 'mc/mark-previous-like-this)

(global-set-key [(shift meta up)] 'move-text-up)
(global-set-key [(shift meta down)] 'move-text-down)

(define-key custom-bindings-map (kbd "C-c m") 'magit-status)

(global-set-key (kbd "C-c <f8>") 'neotree-toggle)
(global-set-key (kbd "C-c <f7>") 'neotree-project-dir-toggle)

(global-set-key (kbd "<f8>") 'treemacs)

(global-set-key (kbd "C-c A") 'org-agenda)
(global-set-key (kbd "<f2>") 'org-beamer-export-to-pdf)
(define-key org-mode-map (kbd "ESC <up>") 'org-metaup)
(define-key org-mode-map (kbd "ESC <down>") 'org-metadown)

(eval-after-load "dired" '(progn
                            ;;(define-key dired-sidebar-mode-map (kbd "<tab>") 'dired-subtree-cycle)
                            (define-key dired-mode-map (kbd "<tab>") 'dired-subtree-cycle)
                            (define-key dired-mode-map "i" 'dired-subtree-insert)
                            (define-key dired-mode-map ";" 'dired-subtree-remove)))

(define-key custom-bindings-map (kbd "C-x C-n") 'dired-sidebar-toggle-sidebar)
(define-key custom-bindings-map (kbd "C-x C-m") 'dired-sidebar-toggle-with-current-directory)

(global-set-key (kbd "M-s s") 'sr-speedbar-toggle)

(define-key company-active-map (kbd "C-d") 'company-show-doc-buffer)
   (define-key company-active-map (kbd "C-n") 'company-select-next)
   (define-key company-active-map (kbd "C-p") 'company-select-previous)
   (define-key company-active-map (kbd "<tab>") 'company-complete)
;;   (define-key company-mode-map (kbd "C-:") 'helm-company)
;;   (define-key company-active-map (kbd "C-:") 'helm-company)

(define-key custom-bindings-map (kbd "C-c h")   'helm-command-prefix)
(define-key custom-bindings-map (kbd "M-x")     'helm-M-x)
(define-key custom-bindings-map (kbd "M-y")     'helm-show-kill-ring)
(define-key custom-bindings-map (kbd "C-x b")   'helm-mini)
(define-key custom-bindings-map (kbd "C-x C-f") 'helm-find-files)
(define-key custom-bindings-map (kbd "C-c h d") 'helm-dash-at-point)
(define-key custom-bindings-map (kbd "C-c h o") 'helm-occur)
(define-key custom-bindings-map (kbd "C-c h g") 'helm-google-suggest)
(define-key custom-bindings-map (kbd "M-i")     'helm-swoop)
(define-key custom-bindings-map (kbd "M-I")     'helm-multi-swoop-all)
(define-key custom-bindings-map (kbd "M-s-n") 'helm-projectile-find-file)
(define-key custom-bindings-map (kbd "M-s-~") 'helm-projectile-find-file)
(global-set-key (kbd "M-s-n") 'helm-projectile-find-file)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-i")   'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z")   'helm-select-action)
(define-key helm-map (kbd "M-a")   'helm-next-source)
(define-key helm-map (kbd "M-e")   'helm-previous-source)
(define-key helm-projectile-find-file-map (kbd "M-a")   'helm-next-source)
(define-key helm-projectile-find-file-map (kbd "M-e")   'helm-previous-source)

(define-key custom-bindings-map (kbd "C-c s") 'projectile-ag)

(global-set-key (kbd "C-M-ш") 'flyspell-auto-correct-word)

(define-key org-mode-map (kbd "<f5>")         'ibuffer)
(define-key custom-bindings-map (kbd "<f5>")         'ibuffer)
(define-key custom-bindings-map (kbd "M-u")         'upcase-dwim)
(define-key custom-bindings-map (kbd "M-m")         'back-to-indentation)
(define-key custom-bindings-map (kbd "M-c")         'capitalize-dwim)
(define-key custom-bindings-map (kbd "M-l")         'downcase-dwim)
(define-key custom-bindings-map (kbd "M-]")         'other-frame)
(define-key custom-bindings-map (kbd "M-[")         (lambda () (interactive) (other-frame -1)))
;;(define-key custom-bindings-map (kbd "M-[")         '(lambda () (interactive) (other-frame -1)))
(define-key custom-bindings-map (kbd "C-j")         'newline-and-indent)
(define-key custom-bindings-map (kbd "C-c s")       'ispell-word)
(define-key custom-bindings-map (kbd "C-c c")       'org-capture)
(define-key custom-bindings-map (kbd "s-a")         'mark-whole-buffer)
(define-key custom-bindings-map (kbd "s-/")         'comment-or-uncomment-region-or-line)
(define-key custom-bindings-map (kbd "C-x m")       'mu4e)
(define-key custom-bindings-map (kbd "C-c <up>")    'windmove-up)
(define-key custom-bindings-map (kbd "C-c <down>")  'windmove-down)
(define-key custom-bindings-map (kbd "C-c <left>")  'windmove-left)
(define-key custom-bindings-map (kbd "C-c <right>") 'windmove-right)
   ;;;; (define-key custom-bindings-map (kbd "C-c t")
   ;;;;     (lambda () (interactive) (org-agenda nil "n")))
(global-set-key (kbd "s-`") 'other-frame)

(define-key global-map  (kbd "M-p")     'jump-to-previous-like-this)
(define-key global-map  (kbd "M-n")     'jump-to-next-like-this)
(define-key global-map  (kbd "C-x 1")   'toggle-maximize-buffer)

;;(define-key custom-bindings-map (kbd "M-,")     'jump-to-previous-like-this)
;;(define-key custom-bindings-map (kbd "M-.")     'jump-to-next-like-this)
(define-key custom-bindings-map (kbd "C-c .")   (cycle-themes))
(define-key custom-bindings-map (kbd "C-c C-f")   (cycle-fonts))
(which-key-add-key-based-replacements "C-c ." "cycle themes")
(define-key custom-bindings-map (kbd "C-x k")   'kill-this-buffer-unless-scratch)
(define-key custom-bindings-map (kbd "s-0") 'global-scale-default)
(define-key custom-bindings-map (kbd "s-=") 'global-scale-up)
(define-key custom-bindings-map (kbd "s-+") 'global-scale-up)
(define-key custom-bindings-map (kbd "s--") 'global-scale-down)
(define-key custom-bindings-map (kbd "C-c j")   'cycle-spacing-delete-newlines)
(define-key custom-bindings-map (kbd "C-c d")   'duplicate-thing)
;;(define-key custom-bindings-map (kbd "<C-tab>") 'tidy)
(define-key custom-bindings-map (kbd "<C-tab>") 'next-buffer)
(define-key custom-bindings-map (kbd "<C-S-tab>") 'previous-buffer)
(define-key custom-bindings-map (kbd "M-§")     'toggle-shell)
;;(define-key custom-bindings-map (kbd "M-m '")     'ansi-term)

(dolist (n (number-sequence 1 9))
  (global-set-key (kbd (concat "M-" (int-to-string n)))
                  (lambda () (interactive) (switch-shell n))))

(define-key custom-bindings-map (kbd "C-c C-q")
  '(lambda ()
     (interactive)
     (focus-mode 1)
     (focus-read-only-mode 1)))
(with-eval-after-load 'org
  (define-key org-mode-map (kbd "C-'") 'org-sync-pdf))

(global-set-key (kbd "s-d") 'duplicate-line)
(global-set-key (kbd "s-c") 'pbcopy)
(global-set-key (kbd "s-v") 'pbpaste)
(global-set-key (kbd "s-x") 'pbcut)

(global-set-key (kbd "s-?") 'comment-region-wrap)

(define-minor-mode custom-bindings-mode
  "A mode that activates custom-bindings."
  t nil custom-bindings-map)

(define-key custom-bindings-map (kbd "M-p")         'lsp-ui-peek-find-references)
